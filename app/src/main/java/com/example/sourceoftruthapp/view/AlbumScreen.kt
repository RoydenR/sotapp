package com.example.sourceoftruthapp.view

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerBasedShape
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import com.example.sourceoftruthapp.model.local.Album
import com.example.sourceoftruthapp.state.AlbumState
import com.example.sourceoftruthapp.viewModel.AlbumViewModel

@Composable
fun AlbumScreen(
    modifier: Modifier = Modifier
) {
    val albumViewModel: AlbumViewModel = hiltViewModel()
    val albumState: AlbumState by albumViewModel.albums.collectAsState()
    LaunchedEffect(Unit) {
        albumViewModel.displayAlbums()
    }
    LazyColumn(modifier = modifier.fillMaxWidth()) {
        items(albumState.albums) {album: Album ->
            Card(
                Modifier
                    .background(MaterialTheme.colorScheme.tertiary)
                    .padding(8.dp)
                    .clip(RoundedCornerShape(6.dp))) {
                Row(Modifier.fillMaxSize()) {
                    AsyncImage(model = album.url, contentDescription = null)
                    Column(Modifier
                            .fillMaxSize()
                            .padding(6.dp)
                    ) {
                        Text(text = album.title, fontSize = 24.sp)
                    }
                }
            }
        }
    }
}
@Preview
@Composable
fun AlbumScreenPreview() {
    AlbumScreen()
}