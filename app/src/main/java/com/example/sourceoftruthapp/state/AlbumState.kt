package com.example.sourceoftruthapp.state

import com.example.sourceoftruthapp.model.local.Album

data class AlbumState(
    val isLoading: Boolean = false,
    val albums: List<Album> = emptyList(),
    val error: Throwable? = null
)
