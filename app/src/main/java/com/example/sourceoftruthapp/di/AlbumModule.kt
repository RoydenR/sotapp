package com.example.sourceoftruthapp.di

import com.example.sourceoftruthapp.domain.GetAlbumUseCase
import com.example.sourceoftruthapp.model.AlbumRepo
import com.example.sourceoftruthapp.model.remote.AlbumService
import com.example.sourceoftruthapp.model.remote.AlbumService.Companion.BASE_URL
import com.example.sourceoftruthapp.viewModel.AlbumViewModel
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
object AlbumModule {

    @Provides
    @Singleton
    fun providesAlbumService(): AlbumService = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create(AlbumService::class.java)

    @Provides
    @Singleton
    fun providesAlbumRepo(albumService: AlbumService):
            AlbumRepo = AlbumRepo(albumService)

    @Provides
    @Singleton
    fun providesAlbumUseCase(albumRepo: AlbumRepo):
            GetAlbumUseCase = GetAlbumUseCase(albumRepo)

    @Provides
    @Singleton
    fun providesAlbumViewModel(getAlbumUseCase: GetAlbumUseCase):
            AlbumViewModel = AlbumViewModel(getAlbumUseCase)
}