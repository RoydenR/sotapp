package com.example.sourceoftruthapp.domain

import com.example.sourceoftruthapp.model.AlbumRepo
import com.example.sourceoftruthapp.model.local.Album
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GetAlbumUseCase @Inject constructor(private val albumRepo: AlbumRepo) {
    suspend operator fun invoke(): Result<List<Album>> =
        withContext(Dispatchers.IO) {
            return@withContext try {
                val albumResponse = albumRepo.getAlbums()
                Result.success(albumResponse)
            } catch (ex: Exception) {
                Result.failure(ex)
            }
        }
}