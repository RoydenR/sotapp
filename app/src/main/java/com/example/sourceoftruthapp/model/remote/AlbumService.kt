package com.example.sourceoftruthapp.model.remote

import com.example.sourceoftruthapp.model.response.AlbumDTO
import retrofit2.http.GET

interface AlbumService {
    companion object{
        const val BASE_URL = "https://jsonplaceholder.typicode.com/albums/1/"
        const val ALBUM_PATH = "photos"
    }
    @GET(ALBUM_PATH)
    suspend fun getAlbums(): List<AlbumDTO>
}