package com.example.sourceoftruthapp.model

import com.example.sourceoftruthapp.model.remote.AlbumService
import com.example.sourceoftruthapp.model.response.AlbumDTO
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Singleton
class AlbumRepo @Inject constructor(private val albumService: AlbumService) {
    suspend fun getAlbums() = withContext(Dispatchers.IO) {
        albumService.getAlbums().map { albumDTO: AlbumDTO -> AlbumDTO.albumMapper(albumDTO) }
    }
}