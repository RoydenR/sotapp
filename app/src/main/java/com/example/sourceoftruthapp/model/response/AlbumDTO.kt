package com.example.sourceoftruthapp.model.response


import com.example.sourceoftruthapp.model.local.Album
import com.google.gson.annotations.SerializedName

data class AlbumDTO(
    val albumId: Int,
    val id: Int,
    val thumbnailUrl: String,
    val title: String,
    val url: String
) {
    companion object{
        fun albumMapper(albumDTO: AlbumDTO): Album {
            return Album(
                albumId = albumDTO.albumId,
                id = albumDTO.id,
                thumbnailUrl = albumDTO.thumbnailUrl,
                title = albumDTO.title,
                url = albumDTO.url
            )
        }
    }
}