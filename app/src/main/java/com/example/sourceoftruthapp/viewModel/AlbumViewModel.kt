package com.example.sourceoftruthapp.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.sourceoftruthapp.domain.GetAlbumUseCase
import com.example.sourceoftruthapp.model.local.Album
import com.example.sourceoftruthapp.state.AlbumState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

@HiltViewModel
class AlbumViewModel @Inject constructor(private val getAlbumUseCase: GetAlbumUseCase) :
    ViewModel() {

    private val _albums = MutableStateFlow(AlbumState())
    val albums = _albums.asStateFlow()

    fun displayAlbums() {
        viewModelScope.launch(Dispatchers.IO) {
            _albums.value = _albums.value.copy(isLoading = true)
            val result = getAlbumUseCase.invoke()
            if (result.isSuccess) {
                val albumResponse: List<Album> = result.getOrThrow()
                _albums.value = _albums.value.copy(albums = albumResponse)
            } else {
                _albums.value = _albums.value.copy(error = result.exceptionOrNull())
            }
            _albums.value = _albums.value.copy(isLoading = false)
        }
    }
}